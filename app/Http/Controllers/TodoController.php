<?php

namespace App\Http\Controllers;
use App\short;
use App\todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $shortens = short::OrderBy('created_at', 'desc')->get();
        return view('index')->with('shortens', $shortens);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $charEng = "abcdefghijklmnopgrstuvwxyz";
        $num09 = "0123456789";
        $url ="";
        for($i = 0 ; $i<5; $i++){
            $url .= $num09[rand(0,strlen($num09))-1];
        }
        $url .= $charEng[rand(0,strlen($charEng))-1];
        $this->validate($request,
            [
                'longURL' =>'required',
            ]
        );
        $todo = new short();
        $todo->longURL = $request->input('longURL');
        $todo->shortURL = $url;
        $todo->view = 0;
        $todo->save();
        return redirect('/new')->with('success','Success!'.$url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($shorten)
    {
        //
        $shorts = Short::all();
        if(count($shorts)>0){
            foreach ($shorts as $short){
                if ($short->shortURL == $shorten){
                    $short->view += 1;
                    $short->save();
                    return view('redirect')->with('longurl',$short->longURL);
                }
            }
        }
        return view('notfound');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
