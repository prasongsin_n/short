@extends('layouts.app')
@section('content')

    <nav  class="navbar navbar-light" style="background-color: #6A5ACD; font-family: 'Mitr';">
        <a class="navbar-brand" href="/new" style="color: #ffffff;">Short URL</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/new" style="color: #ffffff;" >New Shorten URL<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/" style="color: #ffffff;" >List <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>

    <h1 class="mt-3">List</h1>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Created at</th>
            <th scope="col">Long URL</th>
            <th scope="col">Short URL</th>
            <th scope="col"></th>
            <th scope="col">View</th>


        </tr>
        </thead>
        @if(count($shortens)>0)
            @foreach($shortens as $shorten)
        <tbody>
        <tr>
            <td>{{$shorten->created_at}}</td>
            <td><a href="{{url($shorten->longURL)}}" style="color: blueviolet">
                    <h7>{{$shorten->longURL}}</h7>
                </a></td>
            <td><input id="shorturl{{$shorten->id}}" class="form-control" type="text"
                        value="http://www.short.local/t/{{$shorten->shortURL}}" readonly>
            </td>
            <td><button onclick="copy(this)" value="{{$shorten->id}}" type="button" class="btn btn-primary">copy</button></td>
            <td><p>{{$shorten->view}}</p></td>

        </tr>

        </tbody>
            @endforeach
        @endif
    </table>

    <script>
        function copy(clickedBtn) {
            var id = clickedBtn.value;
            var copyText = document.querySelector('#shorturl'+id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied '+ copyText.value);
        }
    </script>
@endsection
