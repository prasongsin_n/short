{{--{{create.blade.php}}--}}
@extends('layouts.app')
@section('content')


    <nav  class="navbar navbar-light" style="background-color: #6A5ACD; font-family: 'Mitr';">
    <a class="navbar-brand" href="/new" style="color: #ffffff;">Short URL</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/new" style="color: #ffffff;" >Shorten  <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/" style="color: #ffffff;" >List <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
    </nav>
    <br>
    <br>

    <form method="post" action="{{ url('/') }}" enctype="multipart/form-data" style=" border: 1px solid blueviolet;">
        @csrf

        <div class="form-group" style="font-family: 'Mitr';">
            <h3 style="text-align: center;">Paste the URL to be shortened</h3>
            <div class="row justify-content-md-center" style="height: 20px; margin-top: 20px;">
            <input type="text" name="longURL" class="form-control col-8 " placeholder="PASTE LONG URL" value="{{old('content')}}">
            </div>
            <div class="container p-5" >

                <div class="row justify-content-md-center" style="text-align:center; font-family: 'Mitr';">
                    <br>
                    <br>

                    <button type="submit" class="btn btn-outline-primary col-5">CREATE SHORT URL</button>

                </div>

            </div>
        </div>
    </form>
    </div>
@endsection
